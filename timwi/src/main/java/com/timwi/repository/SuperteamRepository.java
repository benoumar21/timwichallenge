package com.timwi.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;



/**
 * Spring Data  repository for the Suprteam entity.
 */
@Repository
public interface SuperteamRepository extends JpaRepository<Superteam, Long> {

	Superteam findOneById(Long id);

}