package com.timwi.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.timwi.entity.Superheros;

/**
 * Spring Data  repository for the Superheros entity.
 */
@Repository
public interface SuperherosRepository extends JpaRepository<Superheros, Long> {
	Superheros findOneById(Long id);

	Superheros findOneByName(String name);
}
