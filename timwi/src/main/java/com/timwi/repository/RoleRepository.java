package com.timwi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.timwi.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>  {
}
