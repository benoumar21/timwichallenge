package com.timwi.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;
import com.timwi.entity.User;
import com.timwi.service.SuperherosService;
import com.timwi.service.UserService;

@RestController
@RequestMapping("/api/superheros")
@CrossOrigin
public class SuperHeroController {
	
	
	private final SuperherosService superherosService;
	private final UserService userService;
		
	public SuperHeroController (SuperherosService superherosService, UserService userService) {
		this.superherosService= superherosService;
		this.userService= userService;
	}
	
	@GetMapping
	public List<Superheros> getAllSuperHeros(){
		return this.superherosService.getAllSuperHeros();
	}
	
	@PostMapping
	public void addSuperHeroInSuperTeam (@RequestBody Superheros superheros) {
		User user = this.userService.getCurrentUser();
		user.getUserTeamFavorite().getLesSuperheros().add(superheros);
	}
	
	@PostMapping("/add")
	public void addSuperHero (@RequestBody Superheros superheros) {
		Superheros sh = this.superherosService.save(superheros);
		User user = this.userService.getCurrentUser();
		user.getUserTeamFavorite().getLesSuperheros().add(sh);
	}
	
	@PutMapping
	public void updateProduit(@RequestBody Superheros superheros) {
//		produitService.updateProduit(produit);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteProduit(@PathVariable Long id) {
//		produitService.deleteProduit(id);
			
		}
	
}
