package com.timwi.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;
import com.timwi.service.SuperteamService;
import com.timwi.service.UserService;

@RestController
@RequestMapping("/api/superteam")
@CrossOrigin
public class SuperTeamController {
	
	
	private final SuperteamService superteamService;
	private final UserService userService;
		
	public SuperTeamController (SuperteamService superteamService, UserService userService) {
		this.superteamService= superteamService;
		this.userService= userService;
	}
	
	@GetMapping
	public List<Superteam> getAllSuperTeams(){
		return this.superteamService.getAllSuperTeams();
	}
	
	@PostMapping
	public void addSuperTeam(@RequestBody Superteam Superteam) {
//		this.userService.addSuperHeroInUserTeam(superheros);
//		Superteam st = this.userService.getCurrentUser().getUserTeamFavorite().addSuperheros(superheros);
//		System.out.println(st.getName());
//		this.superteamService.addSuperHeroInUserTeam(st);
	}

	
	@PutMapping
	public void updateSuperTeam(@RequestBody Superheros superheros) {
//		SuperTeamService.updateSuperTeam(SuperTeam);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteSuperTeam(@PathVariable Long id) {
//		SuperTeamService.deleteSuperTeam(id);
			
		}
	
}
