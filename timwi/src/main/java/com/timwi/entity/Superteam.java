package com.timwi.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * A Superteam.
 */
@Entity
@Table(name = "superteam")
public class Superteam implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue
    @Column(name="SUPERTEAM_ID")
    private Long id;

    @Column(name = "name")
    private String name;

	@ManyToMany(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    private Set<Superheros> lesSuperheros = new HashSet<>();
    
    @OneToOne(mappedBy = "userTeamFavorite")
    private User user;
    
    public Superteam() {
	}
    

	public Superteam(String name, Set<Superheros> lesSuperheros) {
		super();
		this.name = name;
		this.lesSuperheros = lesSuperheros;
	}



	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Superteam name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Superheros> getLesSuperheros() {
		return lesSuperheros;
	}

	public void setLesSuperheros(Set<Superheros> lesSuperheros) {
		this.lesSuperheros = lesSuperheros;
	}

	public Superteam addSuperheros(Superheros superheros) {
        this.lesSuperheros.add(superheros);
        return this;
    }

    public Superteam removeSuperheros(Superheros superheros) {
        this.lesSuperheros.remove(superheros);
        return this;
    }

    public void setSuperheros(Set<Superheros> superheros) {
        this.lesSuperheros = superheros;
    }

    public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Superteam superteam = (Superteam) o;
        if (superteam.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), superteam.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Superteam{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
