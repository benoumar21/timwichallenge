package com.timwi.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * A Superheros.
 */
@Entity
@Table(name = "superheros")
public class Superheros implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="SUPERHEROS_ID")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "vignette")
    private String vignette;

    @ManyToMany(mappedBy = "lesSuperheros", cascade=CascadeType.ALL)
    private Set<Superteam> lesSuperteams = new HashSet<>();
    
    public Superheros() {
	}

	public Superheros(Long id, String name, String description, String vignette) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.vignette = vignette;
	}


	public Superheros(String name, String description, String vignette) {
		this.name = name;
		this.description = description;
		this.vignette = vignette;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Superheros name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Superheros description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVignette() {
        return vignette;
    }

    public Superheros vignette(String vignette) {
        this.vignette = vignette;
        return this;
    }

    public void setVignette(String vignette) {
        this.vignette = vignette;
    }
    
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Superheros superheros = (Superheros) o;
        if (superheros.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), superheros.getId());
    }


	@Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Superheros{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", vignette='" + getVignette() + "'" +
            "}";
    }
}
