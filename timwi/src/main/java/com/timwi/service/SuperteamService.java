package com.timwi.service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;
import com.timwi.entity.User;
import com.timwi.repository.SuperteamRepository;
import com.timwi.repository.UserRepository;



@Service
public class SuperteamService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final SuperteamRepository superteamRepository;
	private final UserRepository userRepository;
	
	public SuperteamService (SuperteamRepository superteamRepository, UserRepository userRepository) {
		this.superteamRepository = superteamRepository;
		this.userRepository = userRepository;
	}
	
	public List<Superteam> getAllSuperTeams() {
        List<Superteam> lesSuperteams = new ArrayList<Superteam>();
//        superteamRepository.findAll().forEach(superteams -> lesSuperteams.add(superteams));
        superteamRepository.findAll();
        return lesSuperteams;
    }
	
	public Superteam getAllsuperTeamId(Long id) {
        return superteamRepository.findOneById(id);
    }
	
	@Transactional
	public void addSuperHeroInUserTeam (Superheros superheros) {
//		Superteam st = this.userService.getCurrentUser().getUserTeamFavorite().addSuperheros(superheros);
//		System.out.println(st.getName());
//		this.superteamService.addSuperHeroInUserTeam(st);
//		this.superteamRepository.save(superteam);
    }

	
}
	