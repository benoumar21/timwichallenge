package com.timwi.service;
import java.io.Serializable;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;
import com.timwi.entity.User;
import com.timwi.repository.SuperteamRepository;
import com.timwi.repository.UserRepository;


@Service
@Transactional(readOnly = true)
public class UserService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final UserRepository userRepository;
	private final SuperteamRepository superteamRepository;
	
	public UserService (UserRepository userRepository,  SuperteamRepository superteamRepository) {
		this.userRepository = userRepository;
		this.superteamRepository = superteamRepository;
	}
	
	@Transactional
	public User getCurrentUser() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();	
		return this.userRepository.findByUsername(username);
	}
	
	@Transactional
	public void addSuperHeroInUserTeam (User user, Superheros superheros) {
		Superteam st = user.getUserTeamFavorite().addSuperheros(superheros);
		System.out.println(st.getName());
		this.userRepository.save(user);
    }
	
}
	