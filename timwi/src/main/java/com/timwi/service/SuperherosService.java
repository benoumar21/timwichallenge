package com.timwi.service;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.timwi.entity.Superheros;
import com.timwi.repository.SuperherosRepository;


@Service
public class SuperherosService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final SuperherosRepository superherosRepository;
	
	public SuperherosService (SuperherosRepository superherosRepository) {
		this.superherosRepository = superherosRepository;
	}
	
	public List<Superheros> getAllSuperHeros() {
        List<Superheros> lesSuperheros = new ArrayList<Superheros>();
        superherosRepository.findAll().forEach(superhero -> lesSuperheros.add(superhero));
        return lesSuperheros;
    }
	
	public Superheros getAllSuperherosId(Long id) {
        return superherosRepository.findOneById(id);
    }
	
	public Superheros getAllSuperherosName(String name) {
        return superherosRepository.findOneByName(name);
    }

	public Superheros save(Superheros superheros) {
		return superherosRepository.save(superheros);
	}
}
	