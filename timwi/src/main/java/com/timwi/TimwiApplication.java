package com.timwi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.timwi.entity.Role;
import com.timwi.entity.Superheros;
import com.timwi.entity.Superteam;
import com.timwi.entity.User;
import com.timwi.repository.RoleRepository;
import com.timwi.repository.SuperherosRepository;
import com.timwi.repository.SuperteamRepository;
import com.timwi.repository.UserRepository;
import com.timwi.util.RoleEnum;

@SpringBootApplication
public class TimwiApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx =  SpringApplication.run(TimwiApplication.class, args);
		SuperherosRepository superherosRepository = ctx.getBean(SuperherosRepository.class);
		SuperteamRepository superteamRepository = ctx.getBean(SuperteamRepository.class);
		
//		Superheros sh = superherosRepository.save(new Superheros( "batman", "batman est mon super héro de tous les temps ","Ma vignette"));
//		Superheros sh1 =  superherosRepository.save(new Superheros( "SuperMan", "SuperMan est mon super héro de tous les temps ","Ma vignette"));
//		Superheros sh2 =  superherosRepository.save(new Superheros( "Dupond", "Dupond est mon super héro de tous les temps ","Ma vignette"));
//		
//		Set<Superheros> lesSuperheros = new HashSet<Superheros>();
//		lesSuperheros.add(sh1);
//		lesSuperheros.add(sh2);
//	
//		Superteam st = superteamRepository.save(new Superteam ("Mon premier thème", lesSuperheros));
//		
//		
//		/*		produitRepository.save(new Produit("stylo", 500, 2.12f));*/
//		
//		RoleRepository roleRepository = ctx.getBean(RoleRepository.class);
//		
//		Role roleUser = new Role(RoleEnum.ROLE_USER);
//		Role roleAdmin = new Role(RoleEnum.ROLE_ADMIN);
//		
//		roleRepository.save(roleUser);
//		roleRepository.save(roleAdmin);
//		
//		UserRepository userRepository = ctx.getBean(UserRepository.class);
//		
//		
//		User user = new User("user", "password", true);
//		user.setRoles(Arrays.asList(roleUser));
//		userRepository.save(user);
//		
//		User admin = new User("admin", "password2", true);
//		admin.setRoles(Arrays.asList(roleUser,roleAdmin));
//		admin.setUserTeamFavorite(st);
//		userRepository.save(admin);
//		
//			
		
	}

}

