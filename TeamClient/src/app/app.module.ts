import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';



import { AppComponent } from './app.component';
import { ProduitComponent } from './produit/produit.component';
import { ProduitMockService } from './produit/produit.mock.service';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {appRoutes, AppRoutingModule} from './app.routing.module';
import { ProduitService } from './produit/produit.service';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AppService } from './app.service';
import { XhrInterceptor } from './xhr.interceptor';
import {SuperHerosComponent} from './super-heros/super-heros.component';
import {SuperTeamComponent} from './super-team/super-team.component';
import {SuperHerosService} from "./super-heros/super-heros.service";
import {SuperTeamService} from "./super-team/super-team.service";
import {ProduitResolver} from "./produit/produit.resolver";
import {NgxPaginationModule} from "ngx-pagination";

const appRoutess: Routes = [
  {
    path: 'superheros',
    component: SuperHerosComponent
  },
  {
    path: 'superteam',
    component: SuperTeamComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProduitComponent,
    NavbarComponent,
    SidebarComponent,
    ContentComponent,
    DashboardComponent,
    LoginComponent,
    HomeComponent,
    SuperHerosComponent,
    SuperTeamComponent
  ],
  imports: [
    BrowserModule,AppRoutingModule,FormsModule, ReactiveFormsModule, HttpClientModule, RouterModule.forRoot(appRoutess),NgxPaginationModule

  ],
  providers: [ProduitMockService,
              ProduitService,
               SuperHerosService,
              SuperTeamService,
              AppService,
              { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true},
              CookieService

            ],
  bootstrap: [AppComponent]
})
export class AppModule { }
