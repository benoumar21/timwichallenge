import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_URLS} from '../config/api.url.config';
import {Observable} from 'rxjs';
import {SuperHeroModel} from '../shared/super-hero';
import {Resolve} from "@angular/router";

@Injectable()
export class SuperHerosService {
  
  constructor(private http: HttpClient){
  }

  getSuperHeros() : Observable<any>{
    return this.http.get(API_URLS.SUPER_HEROS_URL);
  }

  addSuperHeros(superHeroModel:SuperHeroModel): Observable<any>{
    return this.http.post(API_URLS.SUPER_HEROS_URL+ '/add', superHeroModel);
  }

  addSuperHeroInSuperTeam(superHeroModel:SuperHeroModel): Observable<any>{
    return this.http.post(API_URLS.SUPER_HEROS_URL, superHeroModel);
  }
}
