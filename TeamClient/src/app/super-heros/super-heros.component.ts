import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SuperHeroModel} from '../shared/super-hero';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SuperHerosService} from './super-heros.service';
import {Observable} from "rxjs";

@Component({
  selector: 'app-super-heros',
  templateUrl: './super-heros.component.html',
  styleUrls: ['./super-heros.component.css']
})
export class SuperHerosComponent implements OnInit {

  LesSuperheros: SuperHeroModel[];

  SuperHeroForm: FormGroup;

  operation: String = 'add';

  selectedSuperHero: SuperHeroModel;

  constructor(private LesSuperheroservice: SuperHerosService, private fb: FormBuilder, private router: Router){
    this.selectedSuperHero = new SuperHeroModel();
  }

  ngOnInit(){
    this.initSuperHero();
    this.loadLesSuperheros();
  }


  loadLesSuperheros() {
    return this.LesSuperheroservice.getSuperHeros().subscribe(
      data => {this.LesSuperheros = data},
      error => { console.log('An error was occured.')},
      () => { console.log('loading LesSuperheros was done')}
    );
  }

  addSuperHero(){
    //on recupere l'objet SuperHero à partir de SuperHero form
    console.log(this.selectedSuperHero);
    this.LesSuperheroservice.addSuperHeros(this.selectedSuperHero).subscribe(
      res => {
        this.initSuperHero();
        this.loadLesSuperheros();
      }
    ),
      error => { console.log('An error was occured.')};
  }


  addSuperHeroInSuperTeam(){
    //on recupere l'objet SuperHero à partir de SuperHero form
   console.log(this.selectedSuperHero);
    this.LesSuperheroservice.addSuperHeros(this.selectedSuperHero).subscribe(
      res => {
        this.initSuperHero();
        this.loadLesSuperheros();
      }
    ),
    error => { console.log('An error was occured.')};
  }
  updateSuperHero(){

  }

  initSuperHero(){
    this.selectedSuperHero = new SuperHeroModel();
  }
    // deleteSuperHero(){
    //   this.LesSuperheroservice.deleteSuperHeros(this.selectedSuperHero.id).
    //   subscribe(
    //     res =>{
    //       this.selectedSuperHero = new SuperHeroModel();
    //       this.loadLesSuperheros();
    //     }
    //   );
    // }

}
