import { Component, OnInit } from '@angular/core';
import {SuperHeroModel} from "../shared/super-hero";
import {SuperHerosService} from "../super-heros/super-heros.service";
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {SuperTeamService} from "./super-team.service";
import {SuperTeamModel} from "../shared/super-team";

@Component({
  selector: 'app-super-team',
  templateUrl: './super-team.component.html',
  styleUrls: ['./super-team.component.css']
})
export class SuperTeamComponent implements OnInit {

  selectedSuperteam: SuperTeamModel;
  LesSuperTeam: SuperTeamModel;

  constructor(private superTeamService: SuperTeamService, private fb: FormBuilder, private router: Router) {
    this.selectedSuperteam = new SuperTeamModel();
  }

  ngOnInit(){
    this.initSuperHero();
    this.loadLesSuperheros();
  }


  loadLesSuperheros() {
    return this.superTeamService.getSuperTeam().subscribe(
      data => {this.LesSuperTeam = data},
      error => { console.log('An error was occured.')},
      () => { console.log('loading LesSuperheros was done')}
    );
  }

  // addSuperHero(){
  //   //on recupere l'objet SuperHero à partir de SuperHero form
  //   console.log(this.selectedSuperHero);
  //   this.superTeamService.addSuperTeam(this.selectedSuperHero).subscribe(
  //     res => {
  //       this.initSuperHero();
  //       this.loadLesSuperheros();
  //     }
  //   ),
  //     error => { console.log('An error was occured.')};
  // }
  //
  //
  // addSuperHeroInSuperTeam(){
  //   //on recupere l'objet SuperHero à partir de SuperHero form
  //   console.log(this.selectedSuperHero);
  //   this.superTeamService.addSuperHeros(this.selectedSuperHero).subscribe(
  //     res => {
  //       this.initSuperHero();
  //       this.loadLesSuperheros();
  //     }
  //   ),
  //     error => { console.log('An error was occured.')};
  // }
  updateSuperHero(){

  }

  initSuperHero(){
    this.selectedSuperteam = new SuperTeamModel();
  }
}
