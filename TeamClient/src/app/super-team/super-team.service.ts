import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {API_URLS} from "../config/api.url.config";
import {SuperTeamModel} from "../shared/super-team";

@Injectable()
export class SuperTeamService {

  constructor(private http: HttpClient){
  }

  getSuperTeam(): Observable<any>{
    return this.http.get(API_URLS.SUPER_TEAM_URL);
  }

  addSuperTeam(team:SuperTeamModel): Observable<any>{
    return this.http.post(API_URLS.SUPER_TEAM_URL, team);
  }

  updateSuperTeam(team:SuperTeamModel): Observable<any>{
    return this.http.put(API_URLS.SUPER_TEAM_URL, team);
  }

  deleteSuperTeam(id:number): Observable<any>{
    return this .http.delete(API_URLS.SUPER_TEAM_URL+ "/" + id );
  }

}
