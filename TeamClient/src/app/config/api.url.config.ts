

export const API_URLS = {
  PRODUITS_URL: 'http://localhost:9090/api/superheros',
  SUPER_HEROS_URL: 'http://localhost:9090/api/superheros',
  SUPER_TEAM_URL: 'http://localhost:9090/api/superteam',
  USER_URL: 'http://localhost:9090/users/'
};
