import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


import {ProduitComponent} from './produit/produit.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {ProduitResolver} from './produit/produit.resolver';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {SuperHerosComponent} from './super-heros/super-heros.component';
import {SuperTeamComponent} from './super-team/super-team.component';
import {SuperTeamService} from "./super-team/super-team.service";
import {SuperHerosService} from "./super-heros/super-heros.service";

export const appRoutes: Routes = [

  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'produit',
        component: ProduitComponent,
        resolve: {
          produits: ProduitResolver
        },
        outlet: 'contentOutlet'
      },
      {
        path: 'superheros',
        component: SuperHerosComponent,
        resolve: {
          produits: ProduitResolver
        },
        outlet: 'contentOutlet'
      },
      {
        path: 'superteam',
        component: SuperTeamComponent,
        outlet: 'contentOutlet'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        outlet: 'contentOutlet'
      },
    ]
  },
  {
    path: 'superheros',
    component: SuperHerosComponent,
    outlet: 'contentOutlet'
  },
  {
    path: 'superteam',
    component: SuperTeamComponent,
    outlet: 'contentOutlet'
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes),],
  exports: [RouterModule],
  providers: [ProduitResolver, SuperTeamService, SuperHerosService]

})

export class AppRoutingModule {


}
