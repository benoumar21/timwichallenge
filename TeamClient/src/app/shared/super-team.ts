import {SuperHeroModel} from './super-hero';

export class SuperTeamModel{
  constructor( public id?: number,
    public name?: string,
    public description?: string,
    public superHeroModel?: SuperHeroModel){
              }
}
