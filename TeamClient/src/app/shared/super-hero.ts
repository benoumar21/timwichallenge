export class SuperHeroModel{
  constructor( public id?: number,
    public name?: string,
    public description?: string,
    public vignette?: string){
              }
}
